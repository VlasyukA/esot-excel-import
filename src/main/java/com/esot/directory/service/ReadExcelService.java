package com.esot.directory.service;

import com.esot.directory.model.ColumnDTO;
import com.esot.directory.model.DictionaryDTO;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReadExcelService {

    final static Logger logger = Logger.getLogger(ReadExcelService.class.getSimpleName());

    @Autowired DictionaryService dictionaryService;

    public void parseAndSendExcel(InputStream is, String host, String login, String password) {
        XSSFWorkbook workbook = readWorkbook(is);
        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            readSheet(workbook.getSheetAt(i), host, login, password);
        }
    }

    private void readSheet(XSSFSheet sheet, String host, String login, String password) {
        logger.info("Начинаю парсинг листа: " + sheet.getSheetName());

        List<MetaData> metaDatas = readMetaData(sheet);
        DictionaryDTO dictionaryDTO = new DictionaryDTO().setDictCode(sheet.getSheetName());

        for (int i = 2; i < sheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow currentRow = sheet.getRow(i);
            if(currentRow == null) continue;

            List<ColumnDTO> columnDTOs = readRow(metaDatas, currentRow);
            dictionaryDTO.setColumns(columnDTOs);
            try {
                dictionaryService.sendRecord(dictionaryDTO, host, login, password);
                logger.info("Отправлен объект " + dictionaryDTO);
            } catch (IOException e) {
                logger.error("Ошибка отправки на сервер " + dictionaryDTO + " "+ e);
            }
        }
    }

    private List<ColumnDTO> readRow(List<MetaData> metaDatas, XSSFRow row) {
        logger.info("Начинаю читать строку №" + row.getRowNum());

        List<ColumnDTO> columnDTOs = new ArrayList<ColumnDTO>();
        List<String> rowAsStrings = readRowAsString(row);

        boolean correctRow = isCorrectRow(metaDatas, rowAsStrings);
        if (correctRow) {
            for (MetaData metaData : metaDatas) {
                ColumnDTO columnDTO = new ColumnDTO();
                columnDTO.setCode(metaData.getColumnName());
                for (int i = 0; i < metaData.getColumnsData().size(); i++) {
                    MetaData.MetaColumnData columnData = metaData.getColumnsData().get(i);
                    String columnValue = rowAsStrings.get(metaData.getStartIndex() + i);

                    if (columnData.isUniq()) columnData.getUniqValues().add(columnValue);
                    if (columnData.getLanguage() == null) columnDTO.setValue(columnValue);
                    else {
                        switch (columnData.getLanguage()) {
                            case EN:
                                logger.info("Получаем англисйскую версию значения " + columnValue);
                                columnDTO.setValueEN(columnValue);
                                break;
                            case RU:
                                logger.info("Получаем русскую версию значения " + columnValue);
                                columnDTO.setValueRU(columnValue);
                                break;
                            case KZ:
                                logger.info("Получаем казахскую версию значения " + columnValue);
                                columnDTO.setValueKZ(columnValue);
                                break;
                        }
                    }
                }
                columnDTOs.add(columnDTO);
            }

        }
        else{
            logger.info("Строка №" + row.getRowNum() + " некорректна");
        }

        return columnDTOs;
    }

    private boolean isCorrectRow(List<MetaData> metaDatas, List<String> rowAsStrings) {
        if(rowAsStrings.isEmpty()) return false;

        for (MetaData metaData : metaDatas) {
            for (int i = 0; i < metaData.getColumnsData().size(); i++) {
                MetaData.MetaColumnData columnData = metaData.getColumnsData().get(i);
                if (columnData.isUniq()) {
                    String columnValue = rowAsStrings.get(metaData.getStartIndex() + i);
                    if (columnData.getUniqValues().contains(columnValue)) return false;
                }
            }
        }

        return true;
    }

    private List<String> readRowAsString(XSSFRow row) {
        logger.info("Получаем все значения строки №" + row.getRowNum());
        List<String> values = new ArrayList<String>();
        for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
            values.add(readCellValue(row.getCell(i)));
        }

        return values;
    }

    public List<MetaData> readMetaData(XSSFSheet sheet) {
        logger.info("Начинаем читать мета-данные листа " + sheet.getSheetName());
        List<MetaData> metaDatas = new ArrayList<MetaData>();
        XSSFRow row = sheet.getRow(1);

        MetaData prevMetadata = null;
        for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
            XSSFCell cell = row.getCell(i);
            String currentColumnName = readCellValue(cell);
            boolean isCurrentColumnUniq = readUniqFlag(sheet, i);

            if (prevMetadata == null) prevMetadata = createMetaData(currentColumnName, i, isCurrentColumnUniq);
            else {
                String prevColumnName = prevMetadata.getColumnName();
                Language multiLanguageColumn = isMultiLanguageColumn(prevColumnName, currentColumnName);

                if (multiLanguageColumn == null) {
                    metaDatas.add(prevMetadata);
                    prevMetadata = createMetaData(currentColumnName, i, isCurrentColumnUniq);
                } else {
                    prevMetadata.setMultiLanguage(true);
                    prevMetadata.setColumnSize(prevMetadata.getColumnSize() + 1);
                    prevMetadata.getColumnsData().add(createLanguageColumn(multiLanguageColumn, isCurrentColumnUniq));
                }
            }
        }
        metaDatas.add(prevMetadata);

        return metaDatas;
    }

    private XSSFWorkbook readWorkbook(InputStream file) {
        logger.info("Начинаем читать файл ");
        XSSFWorkbook workbook = null;
        try  {
            workbook = new XSSFWorkbook(file);
        } catch (IOException e) {
            logger.info("Ошибка чтения файла " + e);
            e.printStackTrace();
        }

        return workbook;
    }

    private boolean readUniqFlag(XSSFSheet sheet, int i) {
        logger.info("Начинаем проверять столбцы таблицы на наличие уникальных значений");
        XSSFRow row = sheet.getRow(0);
        String uniqValue = readCellValue(row.getCell(i));
        return "uniq".equals(uniqValue);
    }

    private MetaData.MetaColumnData createLanguageColumn(Language language, boolean isCurrentColumnUniq) {
        MetaData.MetaColumnData columnData = new MetaData.MetaColumnData();
        columnData.setLanguage(language);
        columnData.setUniq(isCurrentColumnUniq);
        return columnData;
    }

    private MetaData createMetaData(String name, int index, boolean isCurrentColumnUniq) {
        logger.info("Создаем мета-данные для колонки " + name);
        MetaData.MetaColumnData columnData = new MetaData.MetaColumnData();
        columnData.setUniq(isCurrentColumnUniq);

        MetaData metaData = new MetaData();
        metaData.setColumnName(name);
        metaData.setStartIndex(index);
        metaData.setColumnSize(1);
        metaData.getColumnsData().add(columnData);
        return metaData;
    }

    private Language isMultiLanguageColumn(String oneColumn, String anotherColumn) {
        logger.info("Начинаем проверять колонку на наличие мультиязыности");
        for (Language language : Language.values()) {
            if (oneColumn.concat(language.name()).equals(anotherColumn)) return language;
        }

        return null;
    }

    private String readCellValue(XSSFCell cell) {
        logger.info("Определяем тип значения " + cell);
        if(cell == null) return "";

        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            double numericCellValue = cell.getNumericCellValue();
            if((numericCellValue % 1) == 0) {
                return String.valueOf((int) numericCellValue);
            }else {
                return String.valueOf(numericCellValue);
            }
        }
        else return cell.getStringCellValue();
    }
}