package com.esot.directory.service.form;

import com.esot.directory.Constants;
import com.esot.directory.model.form.dto.DataDTO;
import com.esot.directory.model.form.dto.Data;
import com.esot.directory.model.form.FormOption;
import com.esot.directory.model.form.MetaDataForm;
import com.esot.directory.model.form.rest.request.FormRequest;
import com.esot.directory.model.form.rest.response.dictionary.DictionaryColumn;
import com.esot.directory.model.form.rest.response.dictionary.DictionaryItem;
import com.esot.directory.model.form.rest.response.dictionary.DictionaryResponse;
import com.esot.directory.model.form.rest.response.dictionary.Value;
import com.esot.directory.service.DictionaryService;
import com.esot.directory.service.FormService;
import com.sun.org.apache.bcel.internal.generic.I2F;
import org.apache.http.auth.AuthenticationException;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ReadExcelFormService {

    final static Logger logger = Logger.getLogger(ReadExcelFormService.class.getSimpleName());

    @Autowired DictionaryService dictionaryService;

    @Autowired FormService formService;


    public void parseAndSendExcel(InputStream is, FormOption formOption) throws IOException, AuthenticationException, URISyntaxException {
        XSSFWorkbook workbook = readWorkbook(is);

        readSheet(workbook.getSheetAt(0), formOption, workbook);
    }

    private XSSFWorkbook readWorkbook(InputStream file) {
        logger.info("Начинаем читать файл ");
        XSSFWorkbook workbook = null;
        try  {
            workbook = new XSSFWorkbook(file);
        } catch (IOException e) {
            logger.error("Ошибка чтения файла " + e);
            e.printStackTrace();
        }

        return workbook;
    }

    private void readSheet(XSSFSheet sheet, FormOption option, XSSFWorkbook workbook) throws IOException, AuthenticationException, URISyntaxException {
        logger.info("Начинаю парсинг листа: " + sheet.getSheetName());

        List<DataDTO> dates = new ArrayList<DataDTO>();

        //Мета-данные для ключа
        MetaDataForm metaData;


        for (int i = 7; i < sheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow currentRow = sheet.getRow(i);
            if(currentRow == null) continue;

            //метод поиска значения (search) по полю field
            String search = getSearchValue(sheet, option.getFormField(), i);
            option.setSearch(search);

            //Ищем колонку с ключем и формируем мета-данные
            metaData = getTableKey(sheet, i);

            //добавили данные из основного листа
            dates.addAll(getData(sheet, i, option, "staticData"));

            //получаем данные из динамических таблиц
            List<DataDTO> dataAppendableTable = getDataAppendableTable(workbook, metaData, option);
            dates.addAll(dataAppendableTable);

            logger.info("Начинаю поиск uuid для табельного номера " + metaData.getCurrentValue());
            //вызываем метод получения uuid
            List<String> uuids = getUUID(option);

            //формируем для каждого uuid свою форму для отправки
            if (uuids.size() != 0) {
                for (String uuid : uuids) {
                    logger.info("Для табельного номера " + metaData.getCurrentValue() + " найден" + " uuid = " +  uuid);

                    Data form = writeForm(option, dates, uuid);

                    formService.sendRecord(form, option.getHost(), option.getLogin(), option.getPassword());
                }
            } else {
                logger.error("uuid для табельного номера " + metaData.getCurrentValue() + " не найден");
            }
        }
    }

    private List<String> getUUID(FormOption option) throws IOException, AuthenticationException, URISyntaxException {
        FormRequest request = new FormRequest();
        request.setFormCode(option.getFormCode());
        request.setField(option.getFormField());
        request.setType(option.getFormType());
        request.setSearch(option.getSearch());
        request.setSearchInRegistry(option.getFormSearchInRegistry());

        return formService.getUUID(request, option.getHost(), option.getLogin(), option.getPassword());
    }

    private String getSearchValue(XSSFSheet sheet, String formField, int currentRowVal) {
        logger.info("Начинаю определение значения search по полю field = " + formField);
        XSSFRow row = sheet.getRow(4);

        for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {

            String value = getStringValue(row.getCell(i));
            if (value != null) {
                if (value.equals(formField)) {

                    int colNum = row.getCell(i).getColumnIndex();
                    XSSFRow rowValue = sheet.getRow(currentRowVal);

                    if (rowValue != null) {
                        XSSFCell cellValue = rowValue.getCell(colNum);

                        if (cellValue != null) {
                            return getStringValue(cellValue);
                        } else {
                            logger.info("Ячейка, строка " + currentRowVal + ", колонка " + colNum + " null");
                        }
                    } else {
                        logger.error("Строка под индексом " + currentRowVal + " null");
                    }
                }
            }
        }

        return null;
    }

    private List<DataDTO> getData(XSSFSheet sheet, int rowNumValue, FormOption option, String typeData) throws IOException {
        logger.info("Program version: " + Constants.APP_VERSION);
        int str = rowNumValue + 1;
        if (typeData.equals("staticData")) {
            logger.info("Начинаем формировать данные для поля data. Основной таблицы, из строки = " + str);
        } else {
            logger.info("Начинаем формировать данные для поля data. Динамической таблицы " + sheet.getSheetName() + " , из строки = " + str);
        }

        List<DataDTO> dates = new ArrayList<DataDTO>();
        int colNum = 0;
        //rowNumValue минимальное значение 7
        int indexAppendRow = rowNumValue - 6;

        for (int i = 0; i < sheet.getRow(rowNumValue).getPhysicalNumberOfCells(); i++) {
            if (sheet.getRow(4).getCell(colNum) != null) {
                if (!(sheet.getRow(4).getCell(colNum) == null || sheet.getRow(4).getCell(colNum).getStringCellValue().equals(""))) {
                    //то это справочник
                    if (sheet.getRow(1) != null && sheet.getRow(1).getCell(colNum) != null && !sheet.getRow(1).getCell(colNum).getStringCellValue().equals("")) {
                        if (!(sheet.getRow(1).getCell(i) == null || sheet.getRow(1).getCell(i).getStringCellValue().equals(""))) {
                            DataDTO data = new DataDTO();

                            //Получаем значение из справочника по алгоритму
                            /**
                             * Получается алгоритм такой. Мы берем строчку 2 (name_of_school), это название справочника.
                             * Далее мы по 8 строчке, (1234фва) ищем строчку в справочнике, по полю из строки 4 (name).
                             * В этой строчке берем значения поля из строки 3 (id). 5 строка это id
                             */
                            String nameDictionary = sheet.getRow(1).getCell(colNum).getStringCellValue();
                            String currentVal = getStringValue(sheet.getRow(rowNumValue).getCell(colNum));
                            String nameColumnCurrentVal = sheet.getRow(3).getCell(colNum).getStringCellValue();
                            String nameColumnSearchVal = sheet.getRow(2).getCell(colNum).getStringCellValue();

                            String key = getDictionaryValue(nameDictionary, currentVal, nameColumnCurrentVal, nameColumnSearchVal, option);

                            //если значение не нашли в справониках, то данное значение не сохраняем в data
                            if (key != null && !"".equals(currentVal)) {
                                if (typeData.equals("staticData")) {
                                    data.setId(sheet.getRow(4).getCell(colNum).getStringCellValue());
                                } else if (typeData.equals("appendable")) {
                                    data.setId(sheet.getRow(4).getCell(colNum).getStringCellValue() + "-b" + indexAppendRow);
                                }
                                data.setType(sheet.getRow(5).getCell(colNum).getStringCellValue());
                                data.setValue(currentVal);
                                data.setKey(key);

                                logger.info("Добавлено значение " + data + ". Таблицы " + sheet.getSheetName() + " ,строки #" + rowNumValue);
                                dates.add(data);
                            } else {
                                logger.error("Поле key в справочнике " + nameDictionary + " не найдено");
                            }
                        }
                    }
                    //это обычное поле
                    else {
                        DataDTO data = new DataDTO();

                        if (typeData.equals("staticData")) {
                            data.setId(sheet.getRow(4).getCell(colNum).getStringCellValue());
                        } else if (typeData.equals("appendable")) {
                            data.setId(sheet.getRow(4).getCell(colNum).getStringCellValue() + "-b" + indexAppendRow);
                        }
                        String type = sheet.getRow(5).getCell(colNum).getStringCellValue();

                        logger.info("Готовимся определять тип значения " + sheet.getRow(rowNumValue).getCell(colNum));

                        String value = getStringValue(sheet.getRow(rowNumValue).getCell(colNum));

                        data.setType(type);
                        data.setValue(value);

                        if (type.equals("date")) {
                            String dateKey = getDateKey(value);
                            if (dateKey != null) {
                                data.setKey(dateKey);
                                dates.add(data);
                                logger.info("Добавлено значение " + data + ". Таблицы " + sheet.getSheetName() + " ,строки #" + rowNumValue);
                            } else {
                                logger.error("Неверный формат даты [" + value + "]. В таблице " + sheet.getSheetName() + " на строке #" + rowNumValue);
                            }

                        } else {
                            dates.add(data);

                            logger.info("Добавленно значение " + data + " в поле data из Таблицы " + sheet.getSheetName());
                        }
                    }
                    colNum++;
                } else {
                    colNum++;
                }
            }
        }

        return dates;
    }

    public String getDateKey(String value) {

        try {
             if (value != null) {
                 Date date = new SimpleDateFormat("dd.MM.yyyy").parse(value);

                 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
                 SimpleDateFormat resultDateFormat = new SimpleDateFormat("yyyy-MM-dd");

                 String str = simpleDateFormat.format(date);

                 if (str.equals(value)) {
                     String dateForm = resultDateFormat.format(date) + " 00:00:00";
                     logger.info("Ключ даты = " + dateForm);
                     return dateForm;
                 } else {
                     return null;
                 }
             }
        } catch (ParseException e) {
            logger.error("Ошибка, неверный формат даты: " + value);
        }

        return null;
    }

    private String getStringValue(XSSFCell cell) {
        logger.info("Определяем тип значения " + cell);
        if(cell == null) {
            logger.error("Cell null");
            return null;
        }
        cell.setCellType(Cell.CELL_TYPE_STRING);
        DataFormatter fmt = new DataFormatter();

        String stringCellValue = fmt.formatCellValue(cell).trim();
        logger.info("Значение " + stringCellValue);
        String stringCellValue = cell.getStringCellValue().trim();
        if (!stringCellValue.equals("")) {
            logger.info("Значение " + stringCellValue);
        }
        else {
            logger.error("Значение пусто в ячейке по индексу " + cell.getColumnIndex());
        }
        return stringCellValue;
    }

    /**
     * Метод поиска значения из справочника
     * @param dictionaryCode Название справочника
     * @param currentVal текущее значение из формы
     * @param nameColumnCurrentVal название колонки в справочнике текуцего значения
     * @param nameColumnSearchVal название колонки в справочнике значения которое ищем
     * @return Возвращает значение из справочника , которое в дальнейщем подставляется в data поля key
     *
     * p.s.
     * id: "name_of_school-b1",
     * type: "listbox",
     * value: "1234фва",
     * key: "4"
     */
    private String getDictionaryValue(String dictionaryCode, String currentVal, String nameColumnCurrentVal, String nameColumnSearchVal, FormOption option) throws IOException {
        logger.info("Начинаем получение поля key из справочника " + dictionaryCode);

        //Получаем json Справочника
        DictionaryResponse dictionary = dictionaryService.getDictionary(dictionaryCode, option.getHost(), option.getLogin(), option.getPassword());

        String columnId = null;
        String columnIdSearchValue = null;
        DictionaryItem currentItem = null;

        if (dictionary != null && dictionary.getColumns() != null) {
            for (DictionaryColumn column : dictionary.getColumns()) {
                if (column.getCode().equals(nameColumnCurrentVal)) {
                    columnId = column.getColumnID();
                } else if (column.getCode().equals(nameColumnSearchVal)) {
                    columnIdSearchValue = column.getColumnID();
                }
            }

            if (dictionary.getItems().size() != 0) {
                for (DictionaryItem item : dictionary.getItems()) {
                    for (Value value : item.getValues()) {
                        if (value.getValue().equalsIgnoreCase(currentVal) && value.getColumnID().equals(columnId)) {
                            currentItem = item;
                            break;
                        }
                    }
                }

                if (currentItem != null) {
                    for (Value value : currentItem.getValues()) {
                        if (value.getColumnID().equals(columnIdSearchValue)) {
                            return value.getValue();
                        }
                    }
                } else {
                    logger.error("Не нашел в справочнике " + dictionaryCode + " соответствие значению " + currentVal);
                }
            }
        }else{
            logger.error("Dictionary не корректный: " + dictionary);
        }

        return null;
    }

    private Data writeForm(FormOption option, List<DataDTO> dates, String uuid) {
        Data form = new Data();

        form.setData(dates);
        form.setFormUUID(option.getFormUUID());
        form.setUuid(uuid);

        return form;
    }

    private List<DataDTO> getDataAppendableTable(XSSFWorkbook workbook, MetaDataForm meta, FormOption option) throws IOException {
        List<DataDTO> dates = new ArrayList<DataDTO>();

        for (int i = 1; i < workbook.getNumberOfSheets(); i++) {
            DataDTO data = new DataDTO();

            XSSFSheet sheet = workbook.getSheetAt(i);
            logger.info("Начинаем добавлять данные из динамической таблицы " + sheet.getSheetName());

            data.setId(sheet.getSheetName());
            data.setType("appendable_table");
            for (int rowData = 7; rowData < sheet.getPhysicalNumberOfRows(); rowData++) {
                XSSFRow currentRow = sheet.getRow(rowData);
                if (currentRow == null) continue;

                XSSFRow rowKey = sheet.getRow(0);
                if (rowKey.getPhysicalNumberOfCells() == 0) {
                    logger.error("Невозможно прочитать или неверный формат динамической таблицы " + sheet.getSheetName());
                }
                for (int j = 0; j < rowKey.getPhysicalNumberOfCells(); j++) {

                    String key = getStringValue(rowKey.getCell(j));
                    String tabNum = getStringValue(currentRow.getCell(j));

                    if (j == 0) {
                        if (rowKey.getCell(j) != null && key.equals("key")) {

                            //если значение на текущей строчке равно значению из мета данных, то это ключ
                            if (tabNum.equals(meta.getCurrentValue())) {
                                data.setData(getData(sheet, rowData, option, "appendable"));
                                break;
                            } else {
                                logger.error("ключ не найден, или валидный: ключ из Основной таблицы " + meta.getCurrentValue() +
                                        ", табельный номер из Динамической таблицы " + tabNum);
                            }
                        } else {
                            logger.error("В первой строчке динамичской таблицы " + sheet.getSheetName() + " не указано ключевое поле");
                        }
                    }
                }
            }

            if (data.getData() != null) {
                dates.add(data);
                logger.info("Добавляем данные "+ data + " в динамическую таблицу " + sheet.getSheetName());
            } else {
                logger.error("В динамическую таблицу " + sheet.getSheetName() + " данные не добавились");
            }
        }

        return dates;
    }

    private MetaDataForm getTableKey(XSSFSheet sheet, int rowVal) {
        logger.info("Начинаю поиск колонки с ключом");
        MetaDataForm meta = new MetaDataForm();

        XSSFRow rowKey = sheet.getRow(0);

        for (int i = 0; i < rowKey.getPhysicalNumberOfCells(); i++) {

            XSSFCell cell = rowKey.getCell(i);

            if (cell != null) {
                String key = getStringValue(cell);
                if (key != null) {
                    if (cell != null && key.equals("key")) {
                        logger.info("Индекс колнки с ключом равен " + cell.getColumnIndex());
                        meta.setColumnKeyNum(cell.getColumnIndex());
                        break;
                    }
                }
            }
        }
        XSSFRow rowValue = sheet.getRow(rowVal);
        String val = null;

        if(rowValue != null) {
            XSSFCell cellValue = rowValue.getCell(meta.getColumnKeyNum());

            if(cellValue != null) {
                val = getStringValue(cellValue);
            }else {
                logger.error("Ячейка, строка " + rowVal + ", колонка " + meta.getColumnKeyNum() + " null");
            }
        }else{
            logger.error("Строка под индексом " + rowVal + " null");
        }

        logger.info("Значение колонки с ключом " + val);
        meta.setCurrentValue(val);

        return meta;
    }
}
