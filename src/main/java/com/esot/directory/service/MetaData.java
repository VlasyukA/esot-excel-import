package com.esot.directory.service;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Accessors(chain = true)
public class MetaData {

    String columnName;
    Integer startIndex;
    Integer columnSize;
    List<MetaColumnData> columnsData = new ArrayList<MetaColumnData>();
    boolean isMultiLanguage;

    @Data
    @Accessors(chain = true)
    static class MetaColumnData {
        boolean uniq;
        Set<String> uniqValues = new HashSet<String>();
        Language language;
    }
}
