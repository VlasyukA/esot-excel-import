package com.esot.directory.service;

import com.esot.directory.model.form.dto.Data;
import com.esot.directory.model.form.rest.request.FormRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.BaseEncoding;
import org.apache.http.auth.AuthenticationException;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.*;
import java.util.Collections;
import java.util.List;

@Service
public class FormService {

    final static Logger logger = Logger.getLogger(FormService.class.getSimpleName());

    private final static String ADD_FORM_URL = "rest/api/asforms/data/save";
    private final static String GET_UUID_URL = "rest/api/asforms/search";

    public void sendRecord(Data data, String host, String login, String password) throws IOException {

        String charset = "UTF-8";
        String formUUID = data.getFormUUID();
        String uuid = data.getUuid();

        String url = host + ADD_FORM_URL;

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(data.getData());

        json = "\"data\": " + json;
        logger.info("Параметра data = " + json);
        String query = String.format("formUUID=%s&uuid=%s&data=%s",
                URLEncoder.encode(formUUID, charset),
                URLEncoder.encode(uuid, charset),
                URLEncoder.encode(json, charset));

        URLConnection connection = new URL(url).openConnection();
        connection.setDoOutput(true); // Triggers POST.
        connection.setRequestProperty("Accept-Charset", charset);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

        String encoded = BaseEncoding.base64().encode((login + ":" + password).getBytes());
        connection.setRequestProperty("Authorization", "Basic " + encoded);


        try {
            OutputStream output = connection.getOutputStream();
            output.write(query.getBytes(charset));
        } catch (IOException ignored) {

        }

        HttpURLConnection conn = ((HttpURLConnection) connection);

        try {
            if (conn.getResponseCode() != 200) {
                logger.error("Ошибка отправки данных на сервер. Synergy response code " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            StringBuffer result = new StringBuffer();

            while ((output = br.readLine()) != null) {
                result.append(output);
            }
            System.out.println(result);
            logger.error("Успешно отправлено на сервер");
        }catch (Exception e) {
            logger.error("Ошибка отправки на сервер: " + e);
        }finally {
            conn.disconnect();
        }

    }

    public List<String> getUUID(FormRequest formRequest, String host, String login, String password) throws IOException, AuthenticationException, URISyntaxException {
        if(formRequest.getFormCode() == null ||
                formRequest.getSearchInRegistry() == null ||
                formRequest.getType() == null  ||
                formRequest.getField() == null ||
                formRequest.getSearch() == null) {
            logger.error("Не валидные данные для получение UUID. " + formRequest);
            return Collections.emptyList();
        }
        URL url = new URL(host + GET_UUID_URL
                + "?formCode=" + URLEncoder.encode(formRequest.getFormCode(), "utf-8")
                + "&searchInRegistry=" + URLEncoder.encode(formRequest.getSearchInRegistry(), "utf-8")
                + "&type=" + URLEncoder.encode(formRequest.getType(), "utf-8")
                + "&field=" + URLEncoder.encode(formRequest.getField(), "utf-8")
                + "&search=" + URLEncoder.encode(formRequest.getSearch(), "utf-8"));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json; charset=utf-8");

        String encoded = BaseEncoding.base64().encode((login + ":" + password).getBytes());
        conn.setRequestProperty("Authorization", "Basic " + encoded);

        String output;
        StringBuffer result = new StringBuffer();

        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

        while ((output = br.readLine()) != null) {
            result.append(output);
        }

        conn.disconnect();
        System.out.println(result.toString());
        ObjectMapper mapper = new ObjectMapper();
        List<String> myObjects = mapper.readValue(result.toString(), mapper.getTypeFactory().constructCollectionType(List.class, String.class));

        return myObjects;
    }
}
