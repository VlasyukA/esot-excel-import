package com.esot.directory.service;

import com.esot.directory.model.DictionaryDTO;
import com.esot.directory.model.form.rest.response.dictionary.DictionaryResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.BaseEncoding;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringWriter;

@Service
public class DictionaryService {

    final static Logger logger = Logger.getLogger(DictionaryService.class.getSimpleName());

    private final static String ADD_DICTIONARY_URL = "rest/api/dictionary/record/add";
    private final static String GET_DICTIONARY_VALUES_URL = "rest/api/dictionary/get_by_code";

    public void sendRecord(DictionaryDTO dictionaryDTO, String host, String login, String password) throws IOException {
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.createDefault();
            String auth = login + ":" + password;
            String authEncoder = BaseEncoding.base64().encode(auth.getBytes("UTF-8"));

            HttpPost post = new HttpPost(host + ADD_DICTIONARY_URL);
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Authorization", "Basic " + authEncoder);
            ObjectMapper objectMapper = new ObjectMapper();

            String json = objectMapper.writeValueAsString(dictionaryDTO);
            post.setEntity(new StringEntity(json, "UTF-8"));
            logger.info("Отправляем JSON: " + json);
            httpClient.execute(post);

        } finally {
            if (httpClient != null) httpClient.close();
        }
    }

    public DictionaryResponse getDictionary(String dictionaryCode, String host, String login, String password) throws IOException {
        CloseableHttpClient httpClient = null;
        try {
            httpClient = HttpClients.createDefault();
            String auth = login + ":" + password;
            String authEncoder = BaseEncoding.base64().encode(auth.getBytes("UTF-8"));

            HttpGet get = new HttpGet(host + GET_DICTIONARY_VALUES_URL + "?dictionaryCode=" + dictionaryCode);
            get.setHeader("Content-Type", "application/json");
            get.setHeader("Authorization", "Basic " + authEncoder);

            CloseableHttpResponse response = httpClient.execute(get);
            StringWriter writer = new StringWriter();
            IOUtils.copy(response.getEntity().getContent(), writer);
            String theString = writer.toString();

            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

            logger.info("Получен справочник от сервера: " + theString);

            try {
                return mapper.readValue(theString, DictionaryResponse.class);
            }catch (Exception e) {
                logger.error("Ошибка парсинга справочника ", e);
                return null;
            }

        } finally {
            if (httpClient != null) httpClient.close();
        }
    }
}
