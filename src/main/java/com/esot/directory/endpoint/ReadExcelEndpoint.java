package com.esot.directory.endpoint;

import com.esot.directory.model.form.FormOption;
import com.esot.directory.service.ReadExcelService;
import com.esot.directory.service.form.ReadExcelFormService;
import org.apache.http.auth.AuthenticationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;

@Controller
public class ReadExcelEndpoint {

    final static Logger logger = Logger.getLogger(ReadExcelEndpoint.class.getSimpleName());

    @Autowired private ReadExcelService readExcelService;
    @Autowired private ReadExcelFormService readExcelFormService;

    @RequestMapping(value = "/dictionary", method = RequestMethod.POST)
    public String uploadFileDictionaryHandler(
                                    @RequestParam("file") MultipartFile multipartFile,
                                    @RequestParam("host") String host,
                                    @RequestParam("login") String login,
                                    @RequestParam("password") String password) throws IOException, AuthenticationException, URISyntaxException {

        if (!multipartFile.isEmpty()) {
            logger.info("Загружен файл " + multipartFile.getName());
            logger.info("Выбран хост " + host);
            logger.info("Выбран login " + login);
            logger.info("Выбран password " + password);

            readExcelService.parseAndSendExcel(multipartFile.getInputStream(), host, login, password);
        }
        else {
            logger.error("Файл " + multipartFile.getName() + " пустой");
        }
        return "redirect:/?message=Import finish";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST, produces = "text/plain;charset=UTF-8")
    public String uploadFileFormHandle(
                                        @RequestParam("file") MultipartFile multipartFile,
                                        @RequestParam("host") String host,
                                        @RequestParam("login") String login,
                                        @RequestParam("password") String password,
                                        @RequestParam("formUUID") String formUUID,
                                        @RequestParam("formCode") String formCode,
                                        @RequestParam("formField") String formField,
                                        @RequestParam("formType") String formType,
                                        @RequestParam("formSearchInRegistry") String formSearchInRegistry) throws IOException, AuthenticationException, URISyntaxException {

        if (!multipartFile.isEmpty()) {
            logger.info("Загружен файл " + multipartFile.getName());
            logger.info("Выбран хост " + host);
            logger.info("Выбран login " + login);
            logger.info("Выбран password " + password);

            FormOption formOption = new FormOption(formUUID, formCode, formField, formType, formSearchInRegistry, host, login, password);
            readExcelFormService.parseAndSendExcel(multipartFile.getInputStream(), formOption);
        }
        else {
            logger.error("Файл " + multipartFile.getName() + " пустой");
        }
        return "redirect:/?message=Import finish";
    }

    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }
}
