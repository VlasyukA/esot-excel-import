package com.esot.directory.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Accessors(chain = true)
@JsonInclude(NON_NULL)
public class ColumnDTO {

    private String code;
    private String value;
    private String valueKZ;
    private String valueRU;
    private String valueEN;
}
