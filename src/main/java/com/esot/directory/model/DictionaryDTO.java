package com.esot.directory.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Data
@Accessors(chain = true)
@JsonInclude(NON_NULL)
public class DictionaryDTO {

    private String dictCode;
    private String itemID;
    private List<ColumnDTO> columns = new ArrayList<ColumnDTO>();
}
