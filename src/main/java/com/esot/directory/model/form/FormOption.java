package com.esot.directory.model.form;

import lombok.Data;

@Data
public class FormOption {
    private String login;
    private String password;
    private String host;
    private String formUUID;
    private String formCode;
    private String formField;
    private String formType;
    private String search;
    private String formSearchInRegistry;

    public FormOption(String formUUID, String formCode, String formField, String formType, String formSearchInRegistry, String host, String login, String password) {
        this.formUUID = formUUID;
        this.formCode = formCode;
        this.formField = formField;
        this.formType = formType;
        this.formSearchInRegistry = formSearchInRegistry;
        this.host = host;
        this.login = login;
        this.password = password;
    }
}
