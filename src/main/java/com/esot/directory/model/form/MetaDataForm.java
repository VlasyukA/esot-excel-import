package com.esot.directory.model.form;

import lombok.Data;

@Data
public class MetaDataForm {
    private int columnKeyNum;
    private String currentValue;
}
