package com.esot.directory.model.form.rest.response;

import lombok.Data;

@Data
public class FormResponse {
    private String[] uuid;
}
