package com.esot.directory.model.form.rest.response.dictionary;

import lombok.Data;

@Data
public class DictionaryColumn {
    private String columnID;
    private String code;
}
