package com.esot.directory.model.form.rest.request;

import lombok.Data;

@Data
public class FormRequest {
    private String formCode;
    private String field;
    private String type;
    private String search;
    private String searchInRegistry;
}
