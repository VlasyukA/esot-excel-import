package com.esot.directory.model.form.rest.response.dictionary;

import lombok.Data;

@Data
public class Value {
    private String value;
    private String columnID;
}
