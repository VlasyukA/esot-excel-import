package com.esot.directory.model.form.dto;

import java.util.List;

@lombok.Data
public class Data {
    private String formUUID;
    private String uuid;
    private List<DataDTO> data;
}
