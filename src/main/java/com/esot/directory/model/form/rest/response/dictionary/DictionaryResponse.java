package com.esot.directory.model.form.rest.response.dictionary;

import lombok.Data;

import java.util.List;

@Data
public class DictionaryResponse {
    private String dictionary_code;
    private String dictionaryID;
    private List<DictionaryColumn> columns;
    private List<DictionaryItem> items;


}
