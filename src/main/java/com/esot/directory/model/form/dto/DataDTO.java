package com.esot.directory.model.form.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataDTO {
    private String id;
    private String type;
    private String value;
    private String key;
    private List<DataDTO> data;
}
