package com.esot.directory.model.form.rest.response.dictionary;

import lombok.Data;

import java.util.List;

@Data
public class DictionaryItem {
    private String itemID;
    private List<Value> values;
}
